<?php 


namespace Illusive;

class Payment {
    
    /**
     * The payment due date
     */ 
    protected $dueDate;
    
    public function getDueDate()
    {
        return $this->dueDate;
    }
    
    public function __construct( $due )
    {
        $this->dueDate = $due;
    }
    
}