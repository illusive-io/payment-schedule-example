<?php 

namespace Illusive;

use Illusive\Payment;
use Carbon\CarbonInterval;
use Carbon\Carbon;

class Schedule {
    
     /**
     * Names of days of the week.
     *
     * @var array
     */
    public $days = array(
        Carbon::SUNDAY => 'Sunday',
        Carbon::MONDAY => 'Monday',
        Carbon::TUESDAY => 'Tuesday',
        Carbon::WEDNESDAY => 'Wednesday',
        Carbon::THURSDAY => 'Thursday',
        Carbon::FRIDAY => 'Friday',
        Carbon::SATURDAY => 'Saturday',
    );
    
    /**
     * The date at which the schedule starts
     */ 
    protected $startDate;
    
    public function getStartDate()
    {
        return $this->startDate;
    }
    
    /**
     * The date at which the schedule ends
     */ 
    protected $expireDate;
    
    public function getExpireDate()
    {
        return $this->expireDate;
    }
    
    /**
     * The date of which the interval is incremented against
     */ 
    protected $intervalDate;
    
    public function getIntervalDate()
    {
        return $this->intervalDate;
    }
    
    /**
     * The period of which a payment is due
     */ 
    protected $term;
    
    public function getTerm()
    {
        return $this->term;
    }
    
    /**
     * The payemnts associated with the schedule
     * @var Illusive\Payment[] $payment
     */ 
    protected $payments;
    
    public function getPayemnts()
    {
        return $this->payments;
    }
    
    public function __construct( $start, $end, $term)
    {
        $this->startDate = $start;
        $this->expireDate = $end;
        $this->term = $term;
        
        $this->intervalDate = $this->startDate;
    }
    
    public function generate()
    {
        $interval = $this->getInterval();
        
        echo '-------------------------------------'. '<br />';
        echo 'Start: '.$this->getStartDate()->format('d-m-Y') . '<br />';
        while( $this->getIntervalDate()->lte($this->getExpireDate()) ){
            $this->intervalDate = $this->getIntervalDate()->add($interval)->copy();   
            echo 'Interval Date: ' . $this->intervalDate->format('d-m-Y'). ' is on a ' . $this->days[$this->intervalDate->dayOfWeek] .'<br />';
            
            if( $this->intervalDate->isWeekDay()){
                $this->payments[] = new Payment($this->intervalDate->copy());    
            }else{
                $this->intervalDate = $this->intervalDate->next(Carbon::MONDAY);
                $this->payments[] = new Payment($this->intervalDate->copy());    
            }
            
        }
        echo 'Finish: ' . $this->getExpireDate()->format('d-m-Y') . '<br />';
        echo '-------------------------------------'. '<br />';
        
        echo '<pre>';
        die(print_r($this->payments,true));
        
        
        //foreach($this->payments as $payment){
        //    echo 'Payment Due Date: ' . $payment->getDueDate()->format('d/m/Y') . ' is on a ' . $this->days[$payment->getDueDate()->dayOfWeek] .'<br />';
        //}
    }
    
    public function getInterval()
    {
        
        //-- 4 Weekly
        if( $this->term == 1){
            return CarbonInterval::weeks(4);
        }
        
        //-- Monthly
        if( $this->term == 2){
            return CarbonInterval::months(1);
        }
        
        //-- Bi Monthly
        if( $this->term == 3){
            return CarbonInterval::months(3);
        }
        
        return false;
        
    }
    
    
}