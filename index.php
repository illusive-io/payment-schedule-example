<?php


/**
 * Payment Scheduling
 * 
 * A schedule must have a start date now or in the future
 * the schedular must calculate the intervals leading up to the date
 * incremented by the term
 * 
 * A payment must not fall on a weekend, if it does backtrack to the next monday.
 * 
 * 
 */ 

error_reporting(E_ALL);
ini_set('display_errors', 1);

require 'vendor/autoload.php';

use Illusive\Schedule;
use Carbon\Carbon;

$start = new Carbon('now');
$end = $start->copy()->addMonths(6);

$schedule = new Illusive\Schedule($start, $end, 2);
$schedule->generate();
